package com.supwisdom.shiji.server.repo;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * @author liuyu
 */
@Data
@TableName("biz_log")
public class BizLogDo {
  /**
   * 主键
   */
  private String id;

  /**
   * 租户
   */
  private String tenant;

  /**
   * prefix+业务主键
   */
  private String bizKey;

  /**
   * 业务主键
   */
  private String bizNo;

  /**
   * 执行人
   */
  private String operator;

  /**
   * 发生了什么
   */
  private String action;

  /**
   * 操作日志的种类
   */
  private String category;

  /**
   * 创建时间
   */
  private LocalDateTime createTime;

  /**
   * 详情
   */
  private String detail;

}
