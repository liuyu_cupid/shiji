package com.supwisdom.shiji.server;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author liuyu
 */
@SpringBootApplication
@MapperScan(basePackages = "com.supwisdom.shiji.server.repo")
public class ShijiServerApplication {

  public static void main(String[] args) {
    SpringApplication.run(ShijiServerApplication.class, args);
  }

}
