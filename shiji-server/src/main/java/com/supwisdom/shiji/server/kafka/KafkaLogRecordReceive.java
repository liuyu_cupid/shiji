package com.supwisdom.shiji.server.kafka;

import com.supwisdom.shiji.beans.LogRecord;
import com.supwisdom.shiji.server.processor.LogRecordProcessor;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class KafkaLogRecordReceive {

  protected ObjectMapper objectMapper = new ObjectMapper();

  @Autowired
  protected LogRecordProcessor logRecordProcessor;

  private final static String TOPIC = "shiji-biz-log";

  @KafkaListener(groupId = "1", topics = TOPIC)
  public void processMessage(ConsumerRecord<String, String> record) {
    try {
      LogRecord logRecord = objectMapper.readValue(record.value(), LogRecord.class);
      logRecordProcessor.process(logRecord);
      log.info("{}", logRecord);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

}