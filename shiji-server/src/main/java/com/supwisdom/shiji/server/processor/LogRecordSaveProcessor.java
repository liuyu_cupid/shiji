package com.supwisdom.shiji.server.processor;

import com.supwisdom.shiji.beans.LogRecord;
import com.supwisdom.shiji.server.repo.BizLogDo;
import com.supwisdom.shiji.server.repo.BizLogMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * @author liuyu
 */
@Component
public class LogRecordSaveProcessor implements LogRecordProcessor {

  @Resource
  protected BizLogMapper bizLogMapper;

  @Override
  public LogRecord process(LogRecord logRecord) {
    BizLogDo bizLogDo = new BizLogDo();
    BeanUtils.copyProperties(logRecord, bizLogDo);
    bizLogMapper.insert(bizLogDo);
    return null;
  }

}
