package com.supwisdom.shiji.server.processor;

import com.supwisdom.shiji.beans.LogRecord;

/**
 * 日志处理
 *
 * @author liuyu
 */
public interface LogRecordProcessor {


  LogRecord process(LogRecord logRecord);

}
