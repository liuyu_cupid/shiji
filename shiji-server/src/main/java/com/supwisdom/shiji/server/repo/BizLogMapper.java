package com.supwisdom.shiji.server.repo;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author liuyu
 */
@Mapper
public interface BizLogMapper extends BaseMapper<BizLogDo> {

}
