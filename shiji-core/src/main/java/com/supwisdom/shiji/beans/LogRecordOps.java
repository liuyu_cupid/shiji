package com.supwisdom.shiji.beans;

import lombok.Builder;
import lombok.Data;

/**
 * @author liuyu
 */
@Data
@Builder
public class LogRecordOps {

    private String successLogTemplate;

    private String failLogTemplate;

    private String operator;

    private String bizKey;

    private String bizNo;

    private String category;

    private String detail;

    private String condition;

}
