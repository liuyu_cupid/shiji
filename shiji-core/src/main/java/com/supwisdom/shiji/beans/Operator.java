package com.supwisdom.shiji.beans;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author liuyu
 * create on 2020/4/29 5:39 下午
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Operator {

    private String name;

}
