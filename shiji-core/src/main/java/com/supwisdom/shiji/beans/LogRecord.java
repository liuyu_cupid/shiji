package com.supwisdom.shiji.beans;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import lombok.*;

import java.time.LocalDateTime;

@Setter
@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class LogRecord {

    /**
     * 主键
     */
    private String id;

    /**
     * 租户
     */
    private String tenant;

    /**
     * prefix+业务主键
     */
    private String bizKey;

    /**
     * 业务主键
     */
    private String bizNo;

    /**
     * 执行人
     */
    private String operator;

    /**
     * 发生了什么
     */
    private String action;

    /**
     * 操作日志的种类
     */
    private String category;

    /**
     * 创建时间
     */
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    private LocalDateTime createTime;

    /**
     * 详情
     */
    private String detail;
}