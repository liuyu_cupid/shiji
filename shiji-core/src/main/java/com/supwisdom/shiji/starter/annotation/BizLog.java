package com.supwisdom.shiji.starter.annotation;

import java.lang.annotation.*;

/**
 * 业务日志
 *
 * @author liuyu
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Documented
public @interface BizLog {

    String success();

    String fail() default "";

    String operator() default "";

    String prefix();

    String bizNo();

    String category() default "";

    String detail() default "";

    String condition() default "";

}
