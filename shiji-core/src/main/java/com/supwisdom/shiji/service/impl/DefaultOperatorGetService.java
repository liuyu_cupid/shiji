package com.supwisdom.shiji.service.impl;


import com.supwisdom.shiji.beans.Operator;
import com.supwisdom.shiji.service.IOperatorGetService;

/**
 * 默认的日志执行人
 *
 * @author liuyu
 */
public class DefaultOperatorGetService implements IOperatorGetService {

    @Override
    public Operator getOperator() {
        Operator operator = new Operator();
        operator.setName("司马迁");
        return operator;
    }

}
