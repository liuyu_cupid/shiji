package com.supwisdom.shiji.service.impl;

import com.supwisdom.shiji.beans.LogRecord;
import com.supwisdom.shiji.service.ILogRecordWriter;
import lombok.extern.slf4j.Slf4j;

/**
 * @author liuyu
 */
@Slf4j
public class DefaultLogRecordWriter implements ILogRecordWriter {

    @Override
    public void record(LogRecord logRecord) {
        log.info("[logRecord] {}", logRecord);
    }

}
