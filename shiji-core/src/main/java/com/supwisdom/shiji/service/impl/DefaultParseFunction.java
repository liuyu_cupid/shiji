package com.supwisdom.shiji.service.impl;


import com.supwisdom.shiji.service.IParseFunction;

/**
 * 默认解析函数
 *
 * @author liuyu
 */
public class DefaultParseFunction implements IParseFunction {

    @Override
    public boolean executeBefore() {
        return true;
    }

    @Override
    public String functionName() {
        return null;
    }

    @Override
    public String apply(String value) {
        return null;
    }

}
