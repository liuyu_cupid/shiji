package com.supwisdom.shiji.service.impl;

import com.supwisdom.shiji.service.IdGenerator;

import java.util.UUID;

/**
 * @author liuyu
 */
public class UUIDGenerator implements IdGenerator {

  @Override
  public String next() {
    return UUID.randomUUID().toString().replaceAll("-", "");
  }

}
