package com.supwisdom.shiji.service;

public interface IFunctionService {

    String apply(String functionName, String value);

    boolean beforeFunction(String functionName);

}
