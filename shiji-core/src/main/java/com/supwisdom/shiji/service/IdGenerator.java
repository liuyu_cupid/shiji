package com.supwisdom.shiji.service;

/**
 * ID生成器
 *
 * @author liuyu
 */
public interface IdGenerator {

  String next();

}
