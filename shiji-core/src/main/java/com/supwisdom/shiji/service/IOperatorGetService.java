package com.supwisdom.shiji.service;


import com.supwisdom.shiji.beans.Operator;

/**
 * @author liuyu
 */
public interface IOperatorGetService {

    /**
     * 可以在里面外部的获取当前登陆的用户，比如UserContext.getCurrentUser()
     *
     * @return 转换成Operator返回
     */
    Operator getOperator();

}
