package com.supwisdom.shiji.service.impl;


import com.supwisdom.shiji.service.IFunctionService;
import com.supwisdom.shiji.service.IParseFunction;

/**
 * 默认解析函数
 *
 * @author liuyu
 */
public class DefaultFunctionService implements IFunctionService {

    private final ParseFunctionStore parseFunctionStore;

    public DefaultFunctionService(ParseFunctionStore parseFunctionStore) {
        this.parseFunctionStore = parseFunctionStore;
    }

    @Override
    public String apply(String functionName, String value) {
        IParseFunction function = parseFunctionStore.getFunction(functionName);
        if (function == null) {
            return value;
        }
        return function.apply(value);
    }

    @Override
    public boolean beforeFunction(String functionName) {
        return parseFunctionStore.isBeforeFunction(functionName);
    }

}
