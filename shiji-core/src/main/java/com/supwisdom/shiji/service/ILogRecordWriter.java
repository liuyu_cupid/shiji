package com.supwisdom.shiji.service;


import com.supwisdom.shiji.beans.LogRecord;

public interface ILogRecordWriter {

    /**
     * 保存log
     *
     * @param logRecord 日志实体
     */
    void record(LogRecord logRecord);

}
