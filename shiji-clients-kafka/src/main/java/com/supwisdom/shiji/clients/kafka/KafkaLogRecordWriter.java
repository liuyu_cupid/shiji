package com.supwisdom.shiji.clients.kafka;

import com.supwisdom.shiji.beans.LogRecord;
import com.supwisdom.shiji.service.ILogRecordWriter;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.core.KafkaTemplate;

@Slf4j
public class KafkaLogRecordWriter implements ILogRecordWriter {

  protected KafkaTemplate<String, String> kafkaTemplate;

  protected ObjectMapper objectMapper;

  private final static String TOPIC = "shiji-biz-log";

  @Override
  public void record(LogRecord logRecord) {
    try {
      kafkaTemplate.send(TOPIC, logRecord.getId(), objectMapper.writeValueAsString(logRecord));
    } catch (JsonProcessingException e) {
      log.error("日志发送失败", e);
    }
  }

  public void setKafkaTemplate(KafkaTemplate<String, String> kafkaTemplate) {
    this.kafkaTemplate = kafkaTemplate;
  }

  public void setObjectMapper(ObjectMapper objectMapper) {
    this.objectMapper = objectMapper;
  }

}