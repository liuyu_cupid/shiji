package com.supwisdom.shiji.clients.autoconfiguration;

import com.supwisdom.shiji.clients.kafka.KafkaLogRecordWriter;
import com.supwisdom.shiji.service.ILogRecordWriter;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.core.KafkaTemplate;

@Configuration
public class KafkaClientAutoConfiguration {

    @Bean
    public ILogRecordWriter logRecordWriter(KafkaTemplate<String, String> kafkaTemplate, ObjectMapper objectMapper) {
        KafkaLogRecordWriter kafkaLogRecordRepository = new KafkaLogRecordWriter();
        kafkaLogRecordRepository.setKafkaTemplate(kafkaTemplate);
        kafkaLogRecordRepository.setObjectMapper(objectMapper);
        return kafkaLogRecordRepository;
    }

}
