package com.supwisdom.shiji.service;


import com.supwisdom.shiji.beans.Student;

/**
 * @author liuyu
 */
public interface IStudentService {

    boolean create(Student student);

    boolean update(Long orderId, Student student);

}
