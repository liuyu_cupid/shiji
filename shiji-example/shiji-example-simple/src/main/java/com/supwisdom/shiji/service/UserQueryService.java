package com.supwisdom.shiji.service;

import org.apache.catalina.User;

import java.util.List;

/**
 * @author liuyu
 */
public interface UserQueryService {

    List<User> getUserList(List<String> userIds);

}
