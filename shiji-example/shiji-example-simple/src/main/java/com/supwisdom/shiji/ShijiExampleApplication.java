package com.supwisdom.shiji;

import com.supwisdom.shiji.starter.annotation.EnableLogRecord;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@SpringBootApplication(exclude = DataSourceAutoConfiguration.class)
@EnableLogRecord(tenant = "example")
@EnableAspectJAutoProxy
public class ShijiExampleApplication {

    public static void main(String[] args) {
        SpringApplication.run(ShijiExampleApplication.class, args);
    }

}
