package com.supwisdom.shiji.service.impl;

import com.supwisdom.shiji.beans.Student;
import com.supwisdom.shiji.constants.LogRecordType;
import com.supwisdom.shiji.context.LogRecordContext;
import com.supwisdom.shiji.service.IStudentService;
import com.supwisdom.shiji.starter.annotation.BizLog;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @author liuyu
 * create on 2020/6/12 11:08 上午
 */
@Service
@Slf4j
public class StudentServiceImpl implements IStudentService {

    @Override
    @BizLog(
            fail = "新建学生，失败原因：「{{#_errorMsg}}」",
            category = "MANAGER_VIEW",
            detail = "{{#student.toString()}}",
            operator = "{{#currentUser}}",
            success = "创建了{{#student.toString()}},测试变量「{{#innerStudent.code}}」,结果:{{#_ret}}",
            prefix = LogRecordType.STUDENT,
            bizNo = "{{#student.code}}")
    public boolean create(Student student) {
        // db insert order
        Student student1 = new Student();
        student1.setCode("B2021");
        LogRecordContext.putVariable("innerStudent", student1);
        return true;
    }

    @Override
    @BizLog(success = "更新了学生{STUDENT{#student.id}}",
            prefix = LogRecordType.STUDENT,
            bizNo = "{{#student.code}}",
            detail = "{{#student.toString()}}")
    public boolean update(Long orderId, Student student) {
        student.setGender("女");
        return false;
    }

}
