package com.supwisdom.shiji.service.impl;

import com.supwisdom.shiji.constants.LogRecordType;
import com.supwisdom.shiji.context.LogRecordContext;
import com.supwisdom.shiji.service.UserQueryService;
import com.supwisdom.shiji.starter.annotation.BizLog;
import org.apache.catalina.User;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author liuyu
 * create on 2021/2/10 11:13 上午
 */
@Service
public class UserQueryServiceImpl implements UserQueryService {

    @Override
    @BizLog(success = "获取用户列表,内层方法调用人{{#user}}", prefix = LogRecordType.STUDENT, bizNo = "{{#user}}")
    public List<User> getUserList(List<String> userIds) {
        LogRecordContext.putVariable("user", "司马迁");
        return null;
    }
}
