package com.supwisdom.shiji.controller;

import com.supwisdom.shiji.beans.Student;
import com.supwisdom.shiji.service.IStudentService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.time.LocalDate;

@RestController
@RequestMapping("")
public class ShijiSimplerController {

    @Resource
    private IStudentService orderService;

    @GetMapping("create")
    public String createOrder() {
        Student student = new Student(1L, "20211001", "张三", "男", LocalDate.now());
        boolean ret = orderService.create(student);
        return "创建成功";
    }


}
