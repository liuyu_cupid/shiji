package com.supwisdom.shiji;

import com.supwisdom.shiji.starter.annotation.EnableLogRecord;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@SpringBootApplication(exclude = DataSourceAutoConfiguration.class)
@EnableLogRecord(tenant = "kafka")
@EnableAspectJAutoProxy
public class ShijiKafkaApplication {

    public static void main(String[] args) {
        SpringApplication.run(ShijiKafkaApplication.class, args);
    }

}
